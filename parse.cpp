#include "asset.hpp"
#include "portfolio.hpp"
#include "parse.hpp"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <Eigen/Dense>

using namespace Eigen;

void splitString(std::string& s, std::vector<std::string>& v, std::string& c){
  std::string::size_type p1, p2;
  p1 = 0;
  p2 = s.find(c);
  while(std::string::npos != p2)
  {
    v.push_back(s.substr(p1, p2-p1));
 
    p1 =p2 + c.size();
    p2 = s.find(c, p1);
  }
  if(p1 != s.length())
    v.push_back(s.substr(p1));
}

std::vector<Asset> RU(char* filename,int &num){
  std::vector<Asset> Aa;
  std::ifstream ifs(filename);
  if(!ifs.is_open()){
    fprintf(stderr,"Error");
       exit(EXIT_FAILURE);
  }
  std::string line;
  std::string c = ",";
  while(std::getline(ifs,line)){
    std::vector<std::string> t;
    splitString(line,t,c);
    if(t.size() != 3){
        fprintf(stderr,"Error")  ;
           exit(EXIT_FAILURE);
    }
    Asset temp;
    temp.name = t[0];
    if(atof(t[1].c_str())==0.0 || atof(t[2].c_str())==0.0){
       fprintf(stderr,"Error");
         exit(EXIT_FAILURE);
    }
    temp.avr = atof(t[1].c_str());
    temp.sigma = atof(t[2].c_str());
    num++;
    Aa.push_back(temp);
  }
  if(num == 0){
     fprintf(stderr,"Error");
    exit(EXIT_FAILURE);
  }
  return Aa;
}

MatrixXd RC(char* filename,int num){
  std::ifstream ifs(filename);
  if(!ifs.is_open()){
     fprintf(stderr,"Error");
       exit(EXIT_FAILURE);
  }

  MatrixXd Corr(num,num);
  std::string line;
  std::string c = ",";
  int i=0;
  while(std::getline(ifs,line)){
    if(i == num){
       fprintf(stderr,"Error");
      exit(EXIT_FAILURE);
    }
    std::vector<std::string> t;
    splitString(line,t,c);
    if(t.size() != (size_t)num){
       fprintf(stderr,"Error");
      exit(EXIT_FAILURE);
    }
    for(int j=0;j<num;j++){
      if(atof(t[j].c_str())==0.0){
         fprintf(stderr,"Error");
           exit(EXIT_FAILURE);
      }
      Corr(i,j) = atof(t[j].c_str());
    }
    i++;
  }
  if(i == 0){
     fprintf(stderr,"Error");
    exit(EXIT_FAILURE);
  }
  for(int i=0;i<num;i++){
    for(int j=0;j<=i;j++){
      if(fabs(Corr(i,j) - Corr(j,i)) > 0.0001 || fabs(Corr(i,j)) > 1.0001){
         fprintf(stderr,"Error");
        exit(EXIT_FAILURE);
      }
    }
    if(fabs(Corr(i,i) - 1) > 0.0001){
       fprintf(stderr,"Error");
      exit(EXIT_FAILURE);
    }
  }
  return Corr;
}
double Unres(Portfolio &P,double &ret){
  int n = P.n;
  MatrixXd A_1 = MatrixXd::Ones(1,n);
  MatrixXd A_2(1,n);
  for(int i=0;i<n;i++){
    A_2(i) = P.A[i].avr;
  }
  MatrixXd A(2,n);
  A<<A_1,A_2;
  MatrixXd B_1 = MatrixXd::Zero(n,1);
  MatrixXd B_2(2,1);
  B_2<<1,ret;
  MatrixXd B(n+2,1);
  B<<B_1,B_2;
  MatrixXd Cov(n,n);
  for(int i=0;i<n;i++){
    for(int j=0;j<n;j++){
      Cov(i,j) = P.A[i].sigma * P.cor(i,j) * P.A[j].sigma;
    }
  }
  MatrixXd O = MatrixXd::Zero(2,2);
  MatrixXd K(n+2,n+2);
  K<<Cov,A.transpose(),A,O;
  VectorXd X(n+2,1);
  X = K.fullPivHouseholderQr().solve(B);
  P.w = X.head(n);
  P.CSigma();
  return P.sigma;
}

double Res(Portfolio &P,double &ret){
  int n = P.n;
  MatrixXd A_1 = MatrixXd::Ones(1,n);
  MatrixXd A_2(1,n);
  for(int i=0;i<n;i++){
    A_2(i) = P.A[i].avr;
  }
  MatrixXd A(2,n);
  A<<A_1,A_2;
  MatrixXd B_1 = MatrixXd::Zero(n,1);
  MatrixXd B_2(2,1);
  B_2<<1,ret;
  MatrixXd B(n+2,1);
  B<<B_1,B_2;
  MatrixXd Cov(n,n);
  for(int i=0;i<n;i++){
    for(int j=0;j<n;j++){
      Cov(i,j) = P.A[i].sigma * P.cor(i,j) * P.A[j].sigma;
    }
  }
  MatrixXd O = MatrixXd::Zero(2,2);
  MatrixXd K(n+2,n+2);
  K<<Cov,A.transpose(),A,O;
  VectorXd X(n+2,1);
  X = K.fullPivHouseholderQr().solve(B);
  MatrixXd A_A = A;
  MatrixXd B_B = B;
  VectorXd X_X = X;
  for(int i=1;;i++){
    int flag = 1;
    MatrixXd C;
        int k=0;
    for(int j=0;j<n;j++){
      if(X_X(j) < 0){
        MatrixXd T = MatrixXd::Zero(n,1);
        T(j,0) = 1;
        MatrixXd temp = C;
        C.resize(n,k+1);
        if(k==0){
          C<<T;
        }
        else{
          C<<temp,T;
        }
        flag = 0;
        k++;
      }
    }
    if(flag == 1){
      break;
    }
    MatrixXd T;
    T = A_A;
    A_A.resize(T.rows() + C.cols(),n);
    A_A<<T,C.transpose();
    T = B_B;
    B_B.resize(T.rows() + C.cols(),1);
    B_B<<T,MatrixXd::Zero(C.cols(),1);
    MatrixXd OO = MatrixXd::Zero(A_A.rows(),A_A.rows());
    MatrixXd KK(Cov.rows() + A_A.rows(),Cov.rows() + A_A.rows());
    KK<<Cov,A_A.transpose(),A_A,OO;
    X_X = KK.fullPivHouseholderQr().solve(B_B);
  }
  P.w = X_X.head(n);
  P.CSigma();
  return P.sigma;
}
