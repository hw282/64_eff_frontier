#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <Eigen/Dense>
#include <iomanip>
#include <unistd.h>

#include "asset.hpp"
#include "parse.hpp"
#include "portfolio.hpp"



int main(int argc, char * argv[]){
  if (argc != 3 && argc !=4) {
    fprintf(stderr, "Error");
    return EXIT_FAILURE;
  }
  if(argc ==4 && strlen(argv[1]) != 2){
    fprintf(stderr, "Error");
    return EXIT_FAILURE;
  }

  int o;
  bool r = false;
  while ((o = getopt(argc, argv, "r")) != -1){
    switch(o){
      case 'r':
        r = true;
        break;
      case '?':
        std::cerr<<"Error";
        return EXIT_FAILURE;
    }
  }
  int n = 0;
  std::vector<Asset> A;
  MatrixXd cor;
  if(r == true){
    A = RU(argv[2],n);
    cor = RC(argv[3],n);
  }
  else{
    A = RU(argv[1],n);
    cor = RC(argv[2],n);
  }
  Portfolio port;
  port.n = n;
  port.A = A;
  port.cor = cor;
  std::cout<<"ROR,volatility"<<std::endl;
  std::cout.setf(std::ios::fixed);
  if(r == false){
    for(double i = 0.01;i <= 0.265;i =i+0.01){
      std::cout<<std::fixed<<std::setprecision(1)<<i*100<<"%,";
      std::cout<<std::fixed<<std::setprecision(2)<<Unres(port,i)*100<<"%"<<std::endl;
    }
  }
  if(r == true){
    for(double j = 0.01;j <= 0.265;j =j+0.01){
      std::cout<<std::fixed<<std::setprecision(1)<<j*100<<"%,";
      std::cout<<std::fixed<<std::setprecision(2)<<Res(port,j)*100<<"%"<<std::endl;
    }
  }
  std::cout.unsetf(std::ios::fixed);
  return EXIT_SUCCESS;
}
