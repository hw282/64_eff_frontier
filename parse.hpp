#ifndef __PARSE_HPP__
#define __PARSE_HPP__

#include "asset.hpp"
#include "portfolio.hpp"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <Eigen/Dense>
using namespace Eigen;

void splitString(std::string& s, std::vector<std::string>& v, std::string& c);
std::vector<Asset> RU(char* filename,int &num);
MatrixXd RC(char* filename,int num);
double Res(Portfolio &P,double &ret);
double Unres(Portfolio &P,double &ret);

#endif
