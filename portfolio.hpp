#ifndef __PORTFOLIO_HPP__
#define __PORTFOLIO_HPP__


#include <cstdio>
#include <cstdlib>
#include <string>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <vector>
#include <Eigen/Dense>

#include "asset.hpp"

using namespace Eigen;

class Portfolio {
public:
  int n;
  VectorXd w;
  std::vector<Asset> A;
  double sigma;
  double ror;
  MatrixXd cor;
  Portfolio(){}
    void CSigma(){
      double s = 0;
      for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
          s=s+w(i)*w(j)*cor(i,j)*A[i].sigma*A[j].sigma;
        }
      }
      sigma = std::sqrt(s);
    }
  ~Portfolio(){}
};

#endif
